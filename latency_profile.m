load("data.dat")

time = data(1,:);
data1 = data(2,:);
data2 = data(3,:);
result1 = cumsum(data1);
result1 = flip(result1);

result2 = cumsum(data2);
result2 = flip(result2);
subplot(2,1,1);
stairs(time,result1);
title('Round-trip time 4 bytes');
ylabel('Latency Profile [messages]');
xlabel('Time [ms]');
set(gca,'YScale','log');
subplot(2,1,2);
stairs(time,result2);
title('Round-trip time 1400 bytes');
ylabel('Latency Profile [messages]');
xlabel('Time [ms]');
set(gca,'YScale','log');