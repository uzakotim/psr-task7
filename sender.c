/* This program sends "Hello world" string to address and port given
 * as command line parameters, then it wait for any response and
 * prints it. */
/* CLIENT */
#include "main.h"
#include <sysLib.h>
#define MEASUREMENTS 10000

int timespec_subtract (struct timespec *result,struct timespec *x,struct timespec *y)
{
	  if (x->tv_nsec < y->tv_nsec) {
			int num_sec = (y->tv_nsec - x->tv_nsec) / 1000000000 + 1;
			y->tv_nsec -= 1000000000 * num_sec;
			y->tv_sec += num_sec;
	  }
	  if (x->tv_nsec - y->tv_nsec > 1000000000) {
			int num_sec = (x->tv_nsec - y->tv_nsec) / 1000000000;
			y->tv_nsec += 1000000000 * num_sec;
			y->tv_sec -= num_sec;
	  }

	  result->tv_sec = x->tv_sec - y->tv_sec;
	  result->tv_nsec = x->tv_nsec - y->tv_nsec;
	
	  return x->tv_sec < y->tv_sec;
}

void udp_latency_sender(char * ip_address, int port, int payload_size, int messages_count)
{
	
	int sockd;
	struct sockaddr_in my_addr, srv_addr;
	char buf[MAX_BUF];
	int count;
	int addrlen;
	
	unsigned long i,j;
	unsigned long long  time_measured;
	int histogram_small[MAX_HISTOGRAM_SIZE];
	int histogram_large[MAX_HISTOGRAM_SIZE];
	char load_small[4];
	char load_large[1400];
	
	for (j=0;j<4;j++)
	{
		load_small[j] = 'a';
		
	}
	for (j=0;j<1400;j++)
	{
		load_large[j] = 'a';
		
	}
	for (j=0;j<MAX_HISTOGRAM_SIZE;j++)
	{
		histogram_small[j] = 0;
		histogram_large[j] = 0;
	}
	
	
	
	printf("Measurement started - client\n");
	sysClkRateSet(CLOCK_RATE);
	
	
	/* Create a UDP socket */
	sockd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockd == -1)
	{
		perror("Socket creation error");
		exit(1);
	}
	
	/* Configure client address */
	my_addr.sin_family = AF_INET;
	my_addr.sin_addr.s_addr = INADDR_ANY;
	my_addr.sin_port = 0;
	
	bind(sockd, (struct sockaddr*)&my_addr, sizeof(my_addr));
	printf("%ld",sysTimestampFreq());
	/* Set server address */
	srv_addr.sin_family = AF_INET;
	inet_aton(ip_address, &srv_addr.sin_addr);
	srv_addr.sin_port = htons(port);
	sysTimestampEnable();
	taskDelay(100);
	for (i = 0;i<MEASUREMENTS;i++)
	{		
		
		strcpy(buf, load_small);
		unsigned long long start=sysTimestamp();
		sendto(sockd, buf, strlen(buf)+1, 0,
		  (struct sockaddr*)&srv_addr, sizeof(srv_addr));
		
		addrlen = sizeof(srv_addr);
		count = recvfrom(sockd, buf, MAX_BUF, 0,
		  (struct sockaddr*)&srv_addr, &addrlen);
		unsigned long long end=sysTimestamp();
		strcpy(buf,"");

		time_measured = 1000000*(end-start)/(2*sysTimestampFreq());
		
		histogram_small[time_measured] += 1;
	}
	taskDelay(100);
	for (i = 0;i<MEASUREMENTS;i++)
	{		
		
		strcpy(buf, load_large);
		unsigned long long start=sysTimestamp();
		sendto(sockd, buf, strlen(buf)+1, 0,
		  (struct sockaddr*)&srv_addr, sizeof(srv_addr));
		
		addrlen = sizeof(srv_addr);
		count = recvfrom(sockd, buf, MAX_BUF, 0,
		  (struct sockaddr*)&srv_addr, &addrlen);
		unsigned long long end=sysTimestamp();
		strcpy(buf,"");
		time_measured = 1000000*(end-start)/(2*sysTimestampFreq());
		
		histogram_large[time_measured] += 1;
	}
	taskDelay(100);
	j = 0;
	while (j<MAX_HISTOGRAM_SIZE-1)
	{
		printf("%0.3f,",j/1000.0);
		j+=1;
	}
	
	printf("%0.3f\n",(MAX_HISTOGRAM_SIZE-1)/1000.0);
	
	j = 0;
	while (j<MAX_HISTOGRAM_SIZE-1)
	{
		printf("%d,",histogram_small[j]);
		j+=1;
	}
	printf("%d\n",histogram_small[j]);
	
	j = 0;
	while (j<MAX_HISTOGRAM_SIZE-1)
	{
		printf("%d,",histogram_large[j]);
		j+=1;
	}
	printf("%d\n",histogram_large[j]);
	
	close(sockd);
	printf("Measurement finished\n");
}
