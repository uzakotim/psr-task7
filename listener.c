/* This program listens at a UDP port specified as command line
 * parameter and once it receives a packet it adds "OK!" and sends it
 * back. */
/* SERVER */
#include "main.h"

void udp_latency(char * ip_address, int port, int payload_size, int messages_count)
{
	printf("Measurement started - server\n");
	int sockd;
	struct sockaddr_in my_name, cli_name;
	char buf[MAX_BUF];
	int status;
	int addrlen;
	
	/* Create a UDP socket */
	sockd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockd == -1)
	{
		perror("Socket creation error");
		exit(1);
	}
	/* Configure server address */
	my_name.sin_family = AF_INET;
	my_name.sin_addr.s_addr = INADDR_ANY;
	my_name.sin_port = htons(port);
	
	
	status = bind(sockd, (struct sockaddr*)&my_name, sizeof(my_name));
	addrlen = sizeof(cli_name);
	for(;;)
	{
		status = recvfrom(sockd, buf, MAX_BUF, 0,
			  (struct sockaddr*)&cli_name, &addrlen);
		strcat(buf, "OK!\n");
		
		status = sendto(sockd, buf, strlen(buf)+1, 0,
		  (struct sockaddr*)&cli_name, sizeof(cli_name));		
	}		
	close(sockd);
	printf("Measurement finished\n");
}
